package priv.mashton.springboot.urlshortener.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import priv.mashton.springboot.urlshortener.jpa.UrlEntity;
import priv.mashton.springboot.urlshortener.jpa.UrlEntityRepository;

import java.util.ArrayList;

import static org.codehaus.groovy.runtime.DefaultGroovyMethods.any;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UrlShortenerServiceTest {

    @Mock
    Base62Encoder encoder;

    @Mock
    UrlEntityRepository repository;

    @InjectMocks
    private UrlShortenerService service = new UrlShortenerService();

    @Test
    public void shortenUrlCreatesCorrectShort() {
        //ARRANGE
        final String baseUri = "localhost:8080/";
        final String url = "http://bbc.co.uk/news/article1";
        final String hash = "b";
        final String expectedShortUrl = baseUri + hash;

        when(repository.findByUrl(url)).thenReturn(new ArrayList<UrlEntity>());

        UrlEntity entity = new UrlEntity(url);
        entity.setId(1L);
        when(repository.save(Matchers.any(UrlEntity.class))).thenReturn(entity);

        when(encoder.encode(anyLong())).thenReturn("b");

        //ACT
        final String shortUrl = service.shortenUrl(baseUri, url);

        //ASSERT
        assertThat(shortUrl, is(equalTo(expectedShortUrl)));
    }



}
