package priv.mashton.springboot.urlshortener.service;


import org.junit.Test;
import org.mockito.InjectMocks;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.isEmptyString;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;

public class Base62EncoderTest {

    final private Base62Encoder encoder = new Base62Encoder();

    @Test
    public void encodeReturnsExpectedValue() {
        //ARRANGE
        final long valueToEncode = 1;
        final String expectedValue = "b";

        //ACT
        String encoded = encoder.encode(valueToEncode);

        //ASSERT
        assertThat(encoded, is(equalTo(expectedValue)));
    }

    @Test
    public void decodeReturnsExpectedValue() {
        //ARRANGE
        final String valueToDecode = "b";
        final long expectedValue = 1;

        //ACT
        long decoded = encoder.decode(valueToDecode);

        //ASSERT
        assertThat(decoded, is(equalTo(expectedValue)));
    }

    @Test
    public void encodeReturnsEmptyStringOnNegativeInput() {
        //ARRANGE
        final long valueToEncode = -1;

        //ACT
        String encoded = encoder.encode(valueToEncode);

        //ASSERT
        assertThat(encoded, isEmptyString());
    }

    @Test
    public void encodeHandlesLargeNumbers() {
        //ARRANGE
        final long valueToEncode = Long.MAX_VALUE;
        final String expectedEncoding = "k9viXaIfiWh";

        //ACT
        String encoded = encoder.encode(valueToEncode);

        //ASSERT
        assertThat(encoded, is(equalTo(expectedEncoding)));
    }

    @Test
    public void decodeHandlesEmptyString() {
        //ARRANGE
        final String valueToDecode = "";
        final long expectedValue = 0;

        //ACT
        long decoded = encoder.decode(valueToDecode);

        //ASSERT
        assertThat(decoded, is(equalTo(expectedValue)));
    }

    @Test
    public void decodeHandlesNullString() {
        //ARRANGE
        final String valueToDecode = null;
        final long expectedValue = 0;

        //ACT
        long decoded = encoder.decode(valueToDecode);

        //ASSERT
        assertThat(decoded, is(equalTo(expectedValue)));
    }








}
