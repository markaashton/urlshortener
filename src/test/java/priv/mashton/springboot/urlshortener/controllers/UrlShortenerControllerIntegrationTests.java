package priv.mashton.springboot.urlshortener.controllers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class UrlShortenerControllerIntegrationTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void clientReturnsShortUrl() {
        //ARRANGE
        final String expectedShort = "http://localhost:8080/b";

        //ACT
        String body = this.restTemplate.postForObject("/api/v1.0?url=http://bbc.co.uk/news/mark", "http://bbc.co.uk/news/mark", String.class);

        //ASSERT
        assertThat(body, is(equalTo(expectedShort)));


    }

}
