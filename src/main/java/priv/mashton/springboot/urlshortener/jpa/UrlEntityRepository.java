package priv.mashton.springboot.urlshortener.jpa;

import org.springframework.data.repository.CrudRepository;
import java.util.List;

public interface UrlEntityRepository extends CrudRepository<UrlEntity, Long> {
    List<UrlEntity> findByUrl(String url);
    List<UrlEntity> findById(Long id);
}
