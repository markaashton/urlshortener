package priv.mashton.springboot.urlshortener.jpa;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class UrlEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String url;

    protected UrlEntity() {
    }

    public UrlEntity(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return String.format("UrlEntity[id=%d, url='%s']", id, url);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}

