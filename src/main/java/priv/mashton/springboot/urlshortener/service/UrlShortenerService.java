package priv.mashton.springboot.urlshortener.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import priv.mashton.springboot.urlshortener.jpa.UrlEntity;
import priv.mashton.springboot.urlshortener.jpa.UrlEntityRepository;

import java.util.List;

@Component
public class UrlShortenerService {

    @Autowired
    UrlEntityRepository urlEntityRepository;

    @Autowired
    Base62Encoder encoder;

    public String findUrl(String baseUri, String shortUrl) {
        String hash = shortUrl.substring(baseUri.length(), shortUrl.length());
        return findUrlByHash(hash);
    }

    public String shortenUrl(String baseUri, String url) {
        List<UrlEntity> urlEntities = urlEntityRepository.findByUrl(url);
        if (urlEntities.isEmpty()) {
            return baseUri + generateNewHash(url);
        }
        return baseUri + encoder.encode(urlEntities.get(0).getId());
    }

    public String findUrlByHash(String hash) {
        long id = encoder.decode(hash);

        List<UrlEntity> urls = urlEntityRepository.findById(id);
        if (urls.isEmpty()) {
            return null;
        }

        return urls.get(0).getUrl();
    }

    private String generateNewHash(String url) {
        UrlEntity urlEntity = new UrlEntity(url);
        urlEntity = urlEntityRepository.save(urlEntity);
        return encoder.encode(urlEntity.getId());
    }


}
