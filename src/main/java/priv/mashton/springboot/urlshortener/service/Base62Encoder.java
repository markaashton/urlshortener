package priv.mashton.springboot.urlshortener.service;

import org.springframework.stereotype.Component;

@Component
public class Base62Encoder {
    private final String characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private final int base = characters.length();

    public String encode(long value){
        StringBuilder result = new StringBuilder();

        while (value > 0){
            Long position = value % base;
            result.append(characters.charAt(position.intValue() % base));
            value /= base;
        }

        return result.reverse().toString();
    }

    public long decode(String value){
        if (value == null) {
            return 0;
        }

        long result = 0;
        for(int pos = 0 ; pos < value.length(); pos++) {
            int charPosition = characters.indexOf(value.charAt(pos));
            result += charPosition * Math.pow(base , (value.length() - (pos + 1)));
        }

        return result;
    }
}
