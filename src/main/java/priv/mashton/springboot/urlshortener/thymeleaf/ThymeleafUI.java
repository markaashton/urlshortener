package priv.mashton.springboot.urlshortener.thymeleaf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;
import priv.mashton.springboot.urlshortener.service.UrlShortenerService;

@Component
public class ThymeleafUI {

    @Autowired
    private UrlShortenerService urlShortenerService;

    @Value("${app.baseuri: http://localhost:8080/}")
    private String BASE_URI;

    private final String USER_INTERFACE_RESOURCE = "user-interface";

    public String getUserInterface() {
        return USER_INTERFACE_RESOURCE;
    }

    public ModelAndView processCreateRequest(BindingResult bindingResult, String url) {
        ModelAndView modelAndView = new ModelAndView("user-interface");

        if (bindingResult.hasErrors()) {
            modelAndView.addObject("errorWithUrl", url);
        }

        String hash = urlShortenerService.shortenUrl(BASE_URI, url);
        return modelAndView.addObject("shortUrlView", hash);
    }

}
