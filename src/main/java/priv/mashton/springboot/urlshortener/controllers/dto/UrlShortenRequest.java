package priv.mashton.springboot.urlshortener.controllers.dto;

import javax.validation.constraints.NotNull;

public class UrlShortenRequest {

    @NotNull
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
