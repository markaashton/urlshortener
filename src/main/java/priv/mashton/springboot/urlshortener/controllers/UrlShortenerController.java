package priv.mashton.springboot.urlshortener.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import priv.mashton.springboot.urlshortener.controllers.dto.UrlShortenRequest;
import priv.mashton.springboot.urlshortener.thymeleaf.ThymeleafUI;
import priv.mashton.springboot.urlshortener.service.UrlShortenerService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

@Controller
public class UrlShortenerController {

    @Autowired
    private ThymeleafUI thymeleafUI;

    @Autowired
    private UrlShortenerService urlShortenerService;

    private final String INVALID_URL_MESSAGE = "Unable to shorten that link. It is not a valid url";
    private final String NOT_FOUND_MESSAGE = "Unable to find link for the url";

    @RequestMapping(value = "/api/v1.0", method = RequestMethod.POST)
    public ResponseEntity<String> shortenUrl(@RequestParam(value = "url") String url,
                                             HttpServletRequest request) {
        if (isInvalidUrl(url)) {
            return ResponseEntity.badRequest().body(INVALID_URL_MESSAGE);
        }

        String baseUri = extractBaseUri(request);
        String shortUrl = urlShortenerService.shortenUrl(baseUri, url);
        return ResponseEntity.ok(shortUrl);
    }

    @RequestMapping(value = "/api/v1.0", method = RequestMethod.GET)
    public ResponseEntity<String> findOriginalUrl(@NotNull @RequestParam(value = "shortUrl") String shortUrl,
                                                  HttpServletRequest request) {
        String baseUri = extractBaseUri(request);
        String url = urlShortenerService.findUrl(baseUri, shortUrl);

        if (url == null || url.isEmpty() ) {
            return ResponseEntity.badRequest().body(NOT_FOUND_MESSAGE);
        }
        return ResponseEntity.ok(url);
    }

    @RequestMapping(path = "/{hash}", method = RequestMethod.GET)
    public void redirect(@PathVariable String hash,
                         HttpServletResponse response) throws IOException {
        String url = urlShortenerService.findUrlByHash(hash);
        if (url == null || url.isEmpty()) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND, NOT_FOUND_MESSAGE);
        } else {
            response.sendRedirect(url);
        }
    }

    @RequestMapping(value="/", method= RequestMethod.GET)
    public String showUserInterface(UrlShortenRequest request) {
        return thymeleafUI.getUserInterface();
    }

    @RequestMapping(value="/", method = RequestMethod.POST)
    public ModelAndView getUserInterface(HttpServletRequest httpRequest,
                                         @Valid UrlShortenRequest request,
                                         BindingResult bindingResult) {
        String url = request.getUrl();
        if (isInvalidUrl(url)) {
            bindingResult.addError(new ObjectError("url", INVALID_URL_MESSAGE));
        }
        return thymeleafUI.processCreateRequest(bindingResult, url);
    }

    private boolean isInvalidUrl(String url) {
        try {
            new URL(url);
        } catch (MalformedURLException e) {
            return true;
        }
        return false;
    }

    private String extractBaseUri(HttpServletRequest request) {
        StringBuffer requestUrl = request.getRequestURL();
        return requestUrl.substring(0, requestUrl.indexOf(request.getRequestURI())) + "/";
    }


}
