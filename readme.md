*Author : Mark Ashton*

*Date : March 2017*

# Requirements
We want you to build a URL shortener, comparable to e.g. https://bitly.com/.
The two functionalities that it has to implement are:

1) As a user, I want to shorten a URL, so I can use this shorten URL in text messages w/o cluttering the message

2) As a user, I want to access the original URL when using the shorten one, so that I can use both URLs interchangeably


You should imagine that your code is going to production and that it should hence satisfy your requirements for such code.

The exercise shouldn't take you more than 4 hours.

# Short overview
The solution is a maven managed, Spring boot microservice, that provides an ReST based API and utilises an in-memory h2 embedded SQL database. New URLs are saved to a database row with two columns (id and url). The short url is a concatenation of the base uri (e.g. http://localhost:8080/) and a Base62 encoding of the numeric id to provide a short url with capacity for many urls.

As a bonus, I provide a user interface (accessible via http://localhost:8080) which is built on Thymeleaf.
# How to build
mvn clean install

# How to run
mvn spring-boot:run

# User Interface
From a browser, navigate to http://localhost:8080

From the user interface, it's possible to submit urls and generate short urls which can be navigated to or selected and copied for further use.

# ReST API
PUT: http://localhost:8080/api/v1.0?url=http://bbc.co.uk/news

GET: http://localhost:8080/api/v1.0?shortUrl=http://localhost:8080/b

REDIRECT: http://localhost:8080/b

# Deployment to a server
The application.properties file contains a configuration for the base uri, currently set to http://localhost:8080/. When deploying to a server, this should be adjusted accordingly.

I ran out of time to devote to covering deployment.

# How I developed the solution
- These are the distinct phases of my development:
    - Analysis of requirements
    - Research (wikipedia, blogs such as coding horror, and various articles describing url shortening algorithms and analysis of url shortening sites such as bitly.com, tinyurl.com etc)
    - Design
    - Development
    - Testing, including execution of automated tests (integration, unit test)
    
# Choice of frameworks
As I am familiar with Spring, I decided on Springboot for rapid development of a microservice which provides a ReST based API, along with a JPA accessible in-memory database (h2).
I also wanted to provide some kind of user interface to enrich the experience and therefore used Thymeleaf, a server-side UI that I have recently been evaluating.

# Solution
For the shortening algorithm I settled on using a Base62 encoding to provide a short "hash" of the provided url based on the id of the database
record. In a production system I would use a real database such as mySQL. The only limitation on storage of url records is memory
in this case, or in a real database, storage capacity.

# Automated Testing
I provide samples of unit testing and integration testing but did not have enough time to implement enough automated testing. I would strongly prefer to add much more.

# What's missing
If I had more time, I would like to do the following:

- Add more unit and integration testing to fully cover all use cases and corner cases
- I currently do no checking of the length of the input url. I would like to do this
- I currently do nothing extra to handle concurrent requests and scale
- I currently do nothing to handle database errors or hitting the limitations of storage
- Provide documentation and detail for deploying to a server
- Optimise the encoding/decoding algorithm
- Consider a different algorith with fixed length and separate processing of domains
- Instead of return a single string for the response (create short url, find by short) return a dto (json) with both full and short variables
- Implement more quality exception handling including return a custom exception for not found short urls
- Tidy up the code (e.g. following advice from Clean Coding)


